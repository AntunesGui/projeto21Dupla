public class Carta {

	public int rank;
	public String naipe;


	public Carta(int rank, String naipe) {
		this.rank = rank;
		this.naipe = naipe;
	}

	public String toString() {
		String nome;

		if(rank == 1) {
			nome = "Ás";
		}else if(rank == 11) {
			nome = "J";
		}else if(rank == 12) {
			nome = "Q";
		}else if (rank == 13){
			nome = "K";
		}else {
			nome = Integer.toString(rank);
		}

		return nome + " de " + naipe;
	}

	public int getValorEfetivo() {
		if(rank == 11 || rank == 12 || rank == 13) {
			return 10;
		}
		return rank;
	}
}